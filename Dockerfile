FROM python:3.10

COPY requirements.txt /
RUN pip install --no-cache-dir -r requirements.txt

COPY src ./bot

VOLUME /bot/database

WORKDIR /bot

ENV PYTHONUNBUFFERED=1

CMD ["python", "bot.py"]
