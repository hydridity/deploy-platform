import discord
import os
import asyncio
import uwuify
from discord.ext import commands

token = os.environ['TOKEN']

uwuid = 944872867487707177
welcomeid = 922152096927744031
registryid = 922153394066567178

intents = discord.Intents.default()
intents.members = True
bot = commands.Bot(command_prefix=".", intents=intents)

@bot.event
async def on_ready():
    print("Ready for action")

@bot.event
async def on_message(message):
    if message.channel.id == uwuid and not message.author.bot:
        msg = f"{message.author.mention}: {uwuify.uwu(message.content)}"
        await message.delete()
        await message.channel.send(msg)
        if message.attachments:
            for x in message.attachments:
                await message.channel.send(x.proxy_url)

    await bot.process_commands(message)

@bot.event
async def on_member_join(member):
    channel = discord.utils.get(member.guild.channels, id=welcomeid)
    registry = discord.utils.get(member.guild.channels, id=registryid)
    await channel.send(f"Welcome to the server {member.mention}!\nPlease head to {registry.mention} and follow the instructions in the pinned message.")

@bot.event
async def on_member_remove(member):
    channel = discord.utils.get(member.guild.channels, id=welcomeid)
    await channel.send(f"{member.mention} has left the server. ☹️")


for filename in os.listdir("./cogs"):
    if filename.endswith(".py"):
        bot.load_extension(f"cogs.{filename[:-3]}")

bot.run(token)
