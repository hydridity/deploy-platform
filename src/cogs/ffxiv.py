import discord
import asyncio
import requests
import os
from pprint import pprint
from datetime import datetime
from tinydb import TinyDB, Query
from discord.ext import commands

# XIVAPI environment variable holds token in format of: private_key=abcdabcd...
key = os.environ['XIVAPI']
db = TinyDB('database/db.json')

permissions = {"developer": [859397433237438514],
               "admin": [922089977142186014]}

roles = {"Member": 922154737871224872}

def rolecheck(user):
    for id in [role.id for role in user.roles]:
        if id in permissions["admin"]:
            return True
    return False

def remove_member_from_db(member):
    q = Query()
    if len(db.search(q.ID == member.id)) != 0:
        db.remove(q.ID == member.id)

class FinalFantasy(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.Cog.listener()
    async def on_ready(self):
        print("FFXIV loaded")

    @commands.Cog.listener()
    async def on_member_remove(self, member):
        remove_member_from_db(member)

    @commands.command()
    async def charinfo(self, ctx, *args):
        if len(args) < 3 and len(args) != 2:
            await ctx.send("Invalid number of arguments. Please specify in order: forename, surname, world")
            return
        elif len(args) == 2:
            world = "ragnarok"
        else:
            world = args[2]

        r = requests.get(f"https://xivapi.com/character/search?name={args[0]}%20{args[1]}&server={world}&{key}")
        data = r.json()
        if "Error" in data:
            if data["Error"] is True:
                await ctx.send(f"ERROR: {data['Subject']}. Please try again in a few hours.")
                print(f"ERROR: {datetime.now().strftime('%Y-%m-%d %H:%M:%S')} {data['Message']}")
                pprint(data)
                return

        if int(data["Pagination"]["Results"]) == 0:
            await ctx.send("Character not found.")
            return

        await asyncio.sleep(1)
        data = data["Results"][0]
        r = requests.get(f"https://xivapi.com/character/{data['ID']}?data=FC&{key}")
        fc = r.json()

        if "Error" in fc:
            if fc["Error"] is True:
                await ctx.send(f"ERROR: {fc['Subject']}. Please try again in a few hours.")
                print(f"ERROR: {datetime.now().strftime('%Y-%m-%d %H:%M:%S')} {fc['Message']}")
                pprint(fc)
                return

        embed = discord.Embed(title=f"{data['Name']}")
        embed.set_thumbnail(url=f"{data['Avatar']}")
        embed.set_footer(text=f"Requested by: {ctx.author.name}", icon_url=ctx.author.avatar_url)
        embed.add_field(name="Server:", value=f"{data['Server']}", inline=False)
        if fc['FreeCompany'] is None:
            embed.add_field(name="Free Company:", value="None", inline=False)
        else:
            embed.add_field(name="Free Company:", value=f"{fc['FreeCompany']['Name']}", inline=False)
        embed.add_field(name="Languages:", value=f"{data['Lang']}", inline=False)
        embed.add_field(name="ID:", value=f"{data['ID']}", inline=False)
        await ctx.send(embed=embed)

    @commands.command()
    async def register(self, ctx, name, surname):
        r = requests.get(f"https://xivapi.com/character/search?name={name}%20{surname}&server=ragnarok&{key}")
        data = r.json()

        if "Error" in data:
            if data["Error"] is True:
                await ctx.send(f"ERROR: {data['Subject']}. Please try again in a few hours.")
                print(f"ERROR: {datetime.now().strftime('%Y-%m-%d %H:%M:%S')} {data['Message']}")
                pprint(data)
                return

        if int(data["Pagination"]["Results"]) == 0:
            await ctx.send("Character not found.")
            return

        char = data["Results"][0]
        q = Query()
        if len(db.search(q.name == char['Name'])) != 0:
            await ctx.send(f"This character is already registered to <@{db.search(q.name == char['Name'])[0]['ID']}>.")
            return

        await asyncio.sleep(1)
        r = requests.get(f"https://xivapi.com/character/{char['ID']}?data=FC&{key}")
        data = r.json()

        if "Error" in data:
            if data["Error"] is True:
                await ctx.send(f"ERROR: {data['Subject']}. Please try again in a few hours.")
                print(f"ERROR: {datetime.now().strftime('%Y-%m-%d %H:%M:%S')} {data['Message']}")
                pprint(data)
                return

        if data['FreeCompany'] is None or data['FreeCompany']['Name'] != "Thistopia":
            await ctx.send(f"{name.capitalize()} {surname.capitalize()} is not a member of Thistopia. Please try again in a few hours, Lodestone takes a while to update")
            return

        await asyncio.sleep(1)
        r = requests.get("https://xivapi.com/freecompany/9237023573225329211?data=FCM&{key}")
        fcm = r.json()

        if "Error" in fcm:
            if fcm["Error"] is True:
                await ctx.send(f"ERROR: {fcm['Subject']}. Please try again in a few hours.")
                print(f"ERROR: {datetime.now().strftime('%Y-%m-%d %H:%M:%S')} {fcm['Message']}")
                pprint(fcm)
                return

        try:
            for member in fcm["FreeCompanyMembers"]:
                if member["Name"].lower() == f"{name.lower()} {surname.lower()}":
                    lodestoneid = member["ID"]
                    break
        except TypeError as e:
            await ctx.send("ERROR: API ERROR. Please try again later")
            print(f"ERROR: {datetime.now().strftime('%Y-%m-%d %H:%M:%S')} {e}")
            return

        embed = discord.Embed(title=f"Registration for {char['Name']}")
        embed.set_thumbnail(url=f"{char['Avatar']}")
        embed.set_footer(text=f"Requested by: {ctx.author.name}", icon_url=ctx.author.avatar_url)
        embed.add_field(name="Server:", value=f"{char['Server']}", inline=False)
        embed.add_field(name="Free Company:", value=f"{data['FreeCompany']['Name']}", inline=False)
        embed.add_field(name="Languages:", value=f"{char['Lang']}", inline=False)
        embed.add_field(name="ID:", value=f"{char['ID']}", inline=False)
        embed.add_field(name="Please wait", value="This registration needs to be manually confirmed by a recruiter")
        msg = await ctx.send(embed=embed)
        await msg.add_reaction('👍')
        await msg.add_reaction('👎')

        def check(reaction, user):
            return rolecheck(user) and (str(reaction.emoji) == '👍' or str(reaction.emoji) == '👎') and reaction.message == msg

        reaction, user = await self.bot.wait_for('reaction_add', check=check)
        if str(reaction.emoji) == '👍':
            db.insert({'name': char['Name'], 'ID': ctx.author.id, 'LID': lodestoneid})
            m = ctx.message.guild.get_member(ctx.author.id)
            role = ctx.message.guild.get_role(roles["Member"])
            await m.add_roles(role)
            await ctx.send(f"{char['Name']} has been registered to {ctx.author.mention} by {user.mention}")
        else:
            await ctx.send(f"{char['Name']} registration to {ctx.author.mention} has been denied by {user.mention}")
        try:
            await msg.delete()
            await ctx.message.delete()
        except Exception as e:
            print(e)

    @commands.command()
    async def unregister(self, ctx, member: discord.Member):
        if not rolecheck(ctx.author):
            await ctx.send("You need to be an officer to use this command.")
            return
        q = Query()
        if len(db.search(q.ID == member.id)) == 0:
            await ctx.send("Character not found in the database.")
            return
        search = db.search(q.ID == member.id)
        m = ctx.message.guild.get_member(search[0]["ID"])
        try:
            toremove = ctx.message.guild.get_role(roles["Member"])
            await m.remove_roles(toremove)
        except Exception as e:
            print(e)
        name = search[0]["name"]
        db.remove(q.ID == member.id)
        await ctx.send(f"{name} has been unregistered from {member.mention} by {ctx.author.mention}.")

    @commands.command()
    async def updatemembers(self, ctx):
        if not rolecheck(ctx.author):
            await ctx.send("You need to be an officer to use this command.")
            return
        r = requests.get("https://xivapi.com/freecompany/9237023573225329211?data=FCM&{key}")
        fcm = r.json()

        if "Error" in fcm:
            if fcm["Error"] is True:
                await ctx.send(f"ERROR: {fcm['Subject']}. Please try again in a few hours.")
                print(f"ERROR: {datetime.now().strftime('%Y-%m-%d %H:%M:%S')} {fcm['Message']}")
                pprint(fcm)
                return

        q = Query()
        try:
            for member in fcm["FreeCompanyMembers"]:
                search = db.search(q.LID == member["ID"])
                if len(search) != 0 and member["Name"] != search[0]["Name"]:
                    try:
                        db.update({'name': member["Name"]}, q.LID == member["ID"])
                    except Exception as e:
                        print(e)
                        return
        except TypeError as err:
            await ctx.send("ERROR: API ERROR. Please try again later.")
            print(f"ERROR: {datetime.now().strftime('%Y-%m-%d %H:%M:%S')} {err}")
            return
        await ctx.send("Members updated.")

    @commands.command()
    async def update_db(self, ctx):
        if not rolecheck(ctx.author):
            await ctx.send("You need to be an officer to use this command.")
            return
        
        records = db.all()

        for record in records:
            found = False
            for member in ctx.author.guild.members:
                if (record["ID"] == member.id) and (ctx.message.guild.get_role(roles["Member"]) in member.roles):
                    found = True
                    break
            if not found:
                q = Query()
                db.remove(q.ID == record["ID"])
                await ctx.send(f"Removed {record['name']} from the database.")
        await ctx.send("Done.")
            

'''
    @commands.command()
    async def teamvalo(self, ctx, command, mention: discord.Member):
        if ctx.author.id not in permissions["valo"]:
            await ctx.send("Only Valo can use this command ^^")
            return
        role = ctx.message.guild.get_role(886383123795935272)
        if command == "add":
            try:
                await mention.add_roles(role)
                await ctx.send("Role added!")
                return
            except Exception as e:
                await ctx.send(e)
                return
        elif command == "remove":
            try:
                await mention.remove_roles(role)
                await ctx.send("Role removed!")
                return
            except Exception as e:
                await ctx.send(e)
                return
        else:
            await ctx.send("Invalid Usage, Try: .teamvalo add/remove @mention")
            return

    @commands.command()
    async def teameznix(self, ctx, command, mention: discord.Member):
        if ctx.author.id not in permissions["eznix"]:
            await ctx.send("Only Eznix can use this command")
            return
        role = ctx.message.guild.get_role(897457935758786590)
        if command == "add":
            try:
                await mention.add_roles(role)
                await ctx.send("Role added!")
                return
            except Exception as e:
                await ctx.send(e)
                return
        elif command == "remove":
            try:
                await mention.remove_roles(role)
                await ctx.send("Role removed!")
                return
            except Exception as e:
                await ctx.send(e)
                return
        else:
            await ctx.send("Invalid Usage, Try: .teameznix add/remove @mention")
            return
'''

def setup(bot):
    bot.add_cog(FinalFantasy(bot))
