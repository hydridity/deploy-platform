import discord
import datetime
import time
from discord.ext import commands

class Info(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.Cog.listener()
    async def on_ready(self):
        print("Info loaded")
        global startTime
        startTime = time.time()

    @commands.command()
    async def uptime(self, ctx):
        uptime = str(datetime.timedelta(seconds=int(round(time.time()-startTime))))
        embed = discord.Embed(title="THIS bot uptime")
        embed.set_footer(text=f"Requested by: {ctx.author.name}", icon_url=ctx.author.avatar_url)
        embed.add_field(name="Uptime", value=uptime, inline=True)
        embed.add_field(name="Started", value=f"{time.strftime('%H:%M:%S', time.localtime(startTime))}", inline=True)
        await ctx.send(embed=embed)


def setup(bot):
    bot.add_cog(Info(bot))
